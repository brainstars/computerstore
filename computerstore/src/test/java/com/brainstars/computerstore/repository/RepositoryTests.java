package com.brainstars.computerstore.repository;

import com.brainstars.computerstore.ProductConstants;
import com.brainstars.computerstore.domain.Product;
import com.brainstars.computerstore.filter.CriteriaOperation;
import com.brainstars.computerstore.filter.ProductSpecification;
import com.brainstars.computerstore.filter.SearchCriteria;
import com.brainstars.computerstore.models.ProductsByCategoryModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static com.brainstars.computerstore.ProductConstants.NAME_KEY;
import static com.brainstars.computerstore.ProductConstants.QUANTITY_KEY;
import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(properties = {"spring.liquibase.enabled=false"})
class RepositoryTests {

    private final Product laptop = new Product(null, ProductConstants.PRODUCT_NAME_1, ProductConstants.CATEGORY_L, ProductConstants.DESCRIPTION, ProductConstants.PRODUCT_QUANTITY, now(), now(), ProductConstants.PRODUCT_VERSION);
    private final Product monitor1 = new Product(null,  ProductConstants.PRODUCT_NAME_2, ProductConstants.CATEGORY_M, ProductConstants.DESCRIPTION, ProductConstants.PRODUCT_QUANTITY, now(), now(), ProductConstants.PRODUCT_VERSION);
    private final Product monitor2 = new Product(null,  ProductConstants.PRODUCT_NAME_3, ProductConstants.CATEGORY_M, ProductConstants.DESCRIPTION, ProductConstants.PRODUCT_QUANTITY, now(), now(), ProductConstants.PRODUCT_VERSION);

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;


    @BeforeEach
    void init()
    {
        entityManager.persist(laptop);
        entityManager.persist(monitor1);
        entityManager.persist(monitor2);
        entityManager.flush();
    }


    @Test
    void whenFindCategories_thenReturnAllCategoriesAndTotal() {


        List<ProductsByCategoryModel> productsByCategories = productRepository.findCategories();

        assertThat(productsByCategories.size()).isEqualTo(2);
        assertThat(productsByCategories.get(0).getProductsAvailable()).isEqualTo(1);
        assertThat(productsByCategories.get(1).getProductsAvailable()).isEqualTo(2);
    }

    @Test
    void whenFindAllByNameEqual_thenReturnProducts()
    {
        List<Product> products = getProducts(NAME_KEY, CriteriaOperation.EQUALS, laptop.getName());

        assertThat(products.size()).isEqualTo(1);
        assertThat(products.get(0)).isEqualTo(laptop);
        assertThat(products.get(0).getName()).isEqualTo(laptop.getName());
    }

    @Test
    void whenFindAllByLessThan_thenReturnFullList()
    {
        List<Product> products = getProducts(QUANTITY_KEY, CriteriaOperation.LESS_THAN, laptop.getQuantity() + 1);

        assertThat(products.size()).isEqualTo(3);
    }

    @Test
    void whenFindAllByLessThan_thenReturnEmptyList()
   {
        List<Product> products = getProducts(QUANTITY_KEY, CriteriaOperation.LESS_THAN, laptop.getQuantity());

        assertThat(products.size()).isZero();
    }

    @Test
    void whenFindAllByGreaterThan_thenReturnFullList()
    {
        List<Product> products = getProducts(QUANTITY_KEY, CriteriaOperation.GREATER_THAN, laptop.getQuantity() -1);

        assertThat(products.size()).isEqualTo(3);
    }

    @Test
    void whenFindAllByGreaterThan_thenReturnEmptyList()
    {
        List<Product> products = getProducts(QUANTITY_KEY, CriteriaOperation.GREATER_THAN, laptop.getQuantity());

        assertThat(products.size()).isZero();
    }

    @Test
    void whenFindAllByContains_thenReturnFullList()
    {
        List<Product> products = getProducts(NAME_KEY, CriteriaOperation.CONTAINS, "CER");

        assertThat(products.size()).isEqualTo(3);
    }

    @Test
    void whenFindAllByStartWith_thenReturnFullList()
    {
        List<Product> products = getProducts(NAME_KEY, CriteriaOperation.STARTS_WITH, "AC");

        assertThat(products.size()).isEqualTo(3);
    }

    @Test
    void whenFindAllByStartWith_thenReturnEmptyList()
    {
        List<Product> products = getProducts(NAME_KEY, CriteriaOperation.STARTS_WITH, "SAM");

        assertThat(products.size()).isZero();
    }

    private List<Product> getProducts(String key, CriteriaOperation operation, Object value) {
        SearchCriteria searchCriteria = new SearchCriteria(key, operation, value);
        ProductSpecification specification = new ProductSpecification(searchCriteria);
        return productRepository.findAll(specification);
    }
}
