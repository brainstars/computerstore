package com.brainstars.computerstore;

public class ProductConstants {
    public static final String PRODUCT_NAME_1 = "ACER_1";
    public static final String PRODUCT_NAME_1_UPDATED = "ACER_1_UPDATED";
    public static final String PRODUCT_NAME_2 = "ACER_2";
    public static final String PRODUCT_NAME_3 = "ACER_3";
    public static final int PRODUCT_QUANTITY = 7;
    public static final int PRODUCT_VERSION = 0;

    public static final String CATEGORY_L = "LAPTOP";
    public static final String CATEGORY_M = "MONITOR";

    public static final String DESCRIPTION = "DESCRIPTION";

    public static final int NON_EXISTENT_PRODUCT_ID = 100;
    public static final long EXISTING_PRODUCT_ID = 1;

    public static final int VALID_QUANTITY = 4;
    public static final int NOT_VALID_QUANTITY = 20;

    public static final String NAME_KEY = "name";
    public static final String QUANTITY_KEY = "quantity";
}
