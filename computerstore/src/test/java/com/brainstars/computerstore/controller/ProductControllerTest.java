package com.brainstars.computerstore.controller;

import com.brainstars.computerstore.controllers.ProductController;
import com.brainstars.computerstore.models.TotalProductsModel;
import com.brainstars.computerstore.models.ProductModel;
import com.brainstars.computerstore.models.ProductsByCategoryModel;
import com.brainstars.computerstore.service.store.StoreService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Arrays;
import java.util.List;

import static com.brainstars.computerstore.PagingConstansts.DIRECTION;
import static com.brainstars.computerstore.PagingConstansts.ORDER_BY;
import static com.brainstars.computerstore.PagingConstansts.PAGE;
import static com.brainstars.computerstore.PagingConstansts.PAGE_SIZE;
import static com.brainstars.computerstore.ProductConstants.CATEGORY_L;
import static com.brainstars.computerstore.ProductConstants.CATEGORY_M;
import static com.brainstars.computerstore.ProductConstants.DESCRIPTION;
import static com.brainstars.computerstore.ProductConstants.EXISTING_PRODUCT_ID;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_1;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_2;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_3;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_QUANTITY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {

    private static final String BASE_CONTROLLER_URL = "/rest-api/products/";
    private static final String CONTENT_TYPE = "application/json";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private StoreService storeService;

    private final List<ProductModel> productModels = Arrays.asList(
            new ProductModel(PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY),
            new ProductModel(PRODUCT_NAME_2, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY),
            new ProductModel(PRODUCT_NAME_3, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY)
    );

    private final List<ProductsByCategoryModel> productsByCategories = Arrays.asList(
            new ProductsByCategoryModel(CATEGORY_L, 3),
            new ProductsByCategoryModel(CATEGORY_M, 8)
    );


    private final ProductModel expected = new ProductModel(PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY);
    private final ProductModel expectedForUpdate = new ProductModel(PRODUCT_NAME_1, null, null, null);

    @Test
    void whenSaveProduct_thenReturnStatusOk() throws Exception {
        sendSaveRequest().andExpect(status().isOk());
    }

    @Test
    void whenSaveProduct_thenReturnCorrectContent() throws Exception {
        when(storeService.save(any(ProductModel.class))).thenReturn(expected);

        ProductModel actual = objectMapper.readValue(sendSaveRequest()
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductModel.class);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void whenGetProduct_thenReturnStatusOk() throws Exception {
        sendGetProductRequest().andExpect(status().isOk());
    }

    @Test
    void whenGetProduct_thenReturnCorrectContent() throws Exception {
        when(storeService.findById(any(Long.class))).thenReturn(expected);

        var actual = objectMapper.readValue(sendGetProductRequest()
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductModel.class);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void whenGetProducts_thenReturnStatusOk() throws Exception {
        sendGetProductsRequest().andExpect(status().isOk());
    }

    @Test
    void whenGetProducts_thenReturnCorrectContent() throws Exception {
        when(storeService.getProducts(any(Integer.class), any(Integer.class), any(String.class), any(String.class), anyList())).thenReturn(productModels);

        final var allProductModels = new TotalProductsModel(productModels.size(), productModels);

        var actual = objectMapper.readValue(sendGetProductsRequest()
                .andReturn()
                .getResponse()
                .getContentAsString(), TotalProductsModel.class);

        assertThat(actual.getTotalRecords()).isEqualTo(allProductModels.getTotalRecords());
        assertThat(actual.getProductModels().size()).isEqualTo(allProductModels.getProductModels().size());
    }

    @Test
    void whenUpdateProduct_thenReturnStatusOk() throws Exception {
        sendUpdateProductRequest().andExpect(status().isOk());
    }

    @Test
    void whenUpdateProduct_thenReturnCorrectContent() throws Exception {
        expected.setName(expectedForUpdate.getName());
        when(storeService.update(EXISTING_PRODUCT_ID, expectedForUpdate)).thenReturn(expected);

        assertThat(objectMapper.readValue(sendUpdateProductRequest().andReturn().getResponse().getContentAsString(), ProductModel.class)).isEqualTo(expected);
    }

    @Test
    void whenDeleteProduct_thenReturnStatusOk() throws Exception {
        mvc.perform(delete(BASE_CONTROLLER_URL + "{id}", EXISTING_PRODUCT_ID)
                .contentType(CONTENT_TYPE))
                .andExpect(status().isOk());

        verify(storeService, times(1)).deleteById(EXISTING_PRODUCT_ID);
    }

    @Test
    void whenOrderProducts_thenReturnProducts() throws Exception {
        sendOrderProductsRequest().andExpect(status().isOk());
    }

    @Test
    void whenOrderProducts_thenReturnCorrectContent() throws Exception {
        when(storeService.orderProduct(any(Long.class), any(Integer.class))).thenReturn(expected);

        assertThat(objectMapper.readValue(sendOrderProductsRequest()
                .andReturn()
                .getResponse()
                .getContentAsString(), ProductModel.class)).isEqualTo(expected);
    }

    @Test
    void whenGetProductsByCategories_thenReturnStatusOk() throws Exception {
        sendGetProductsByCategoriesRequest().andExpect(status().isOk());
    }

    @Test
    void whenGetCategories_thenReturnCorrectContent() throws Exception {
        when(storeService.getProductsByCategory()).thenReturn(productsByCategories);

        var actual = objectMapper.readValue(sendGetProductsByCategoriesRequest()
                .andReturn()
                .getResponse()
                .getContentAsString(), new TypeReference<List<ProductsByCategoryModel>>() {
        });

        assertThat(actual.size()).isEqualTo(productsByCategories.size());

        assertThat(actual.get(0)).isEqualTo(productsByCategories.get(0));
        assertThat(actual.get(0).getCategory()).isEqualTo(productsByCategories.get(0).getCategory());
        assertThat(actual.get(0).getProductsAvailable()).isEqualTo(productsByCategories.get(0).getProductsAvailable());

        assertThat(actual.get(1)).isEqualTo(productsByCategories.get(1));
        assertThat(actual.get(1).getCategory()).isEqualTo(productsByCategories.get(1).getCategory());
        assertThat(actual.get(1).getProductsAvailable()).isEqualTo(productsByCategories.get(1).getProductsAvailable());
    }

    private ResultActions sendSaveRequest() throws Exception {
        return mvc.perform(post(BASE_CONTROLLER_URL)
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(expected)));
    }

    private ResultActions sendGetProductRequest() throws Exception {
        return mvc.perform(get(BASE_CONTROLLER_URL + "{id}", EXISTING_PRODUCT_ID)
                .contentType(CONTENT_TYPE));
    }

    private ResultActions sendGetProductsRequest() throws Exception {
        return mvc.perform(get(BASE_CONTROLLER_URL)
                .contentType(CONTENT_TYPE)
                .param("orderBy", ORDER_BY)
                .param("direction", DIRECTION)
                .param("page", String.valueOf(PAGE))
                .param("pageSize", String.valueOf(PAGE_SIZE)));
    }

    private ResultActions sendUpdateProductRequest() throws Exception {
        return mvc.perform(patch(BASE_CONTROLLER_URL + "{id}", EXISTING_PRODUCT_ID)
                .contentType(CONTENT_TYPE)
                .content(objectMapper.writeValueAsString(expectedForUpdate)));
    }

    private ResultActions sendOrderProductsRequest() throws Exception {
        return mvc.perform(post(BASE_CONTROLLER_URL + "{id}/order/{amount}", EXISTING_PRODUCT_ID, 4)
                .contentType(CONTENT_TYPE));
    }

    private ResultActions sendGetProductsByCategoriesRequest() throws Exception {
        return mvc.perform(get(BASE_CONTROLLER_URL + "categories")
                .contentType(CONTENT_TYPE));
    }
}
