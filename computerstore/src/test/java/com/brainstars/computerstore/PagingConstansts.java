package com.brainstars.computerstore;

public class PagingConstansts {
    public static final int PAGE = 0;
    public static final int PAGE_SIZE = 10;
    public static final String DIRECTION = "ASC";
    public static final String ORDER_BY = "name";
}
