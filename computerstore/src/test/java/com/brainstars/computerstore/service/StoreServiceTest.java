package com.brainstars.computerstore.service;

import com.brainstars.computerstore.domain.Product;
import com.brainstars.computerstore.exeption.InsufficientQuantityException;
import com.brainstars.computerstore.exeption.ProductDoesntExistException;
import com.brainstars.computerstore.models.ProductModel;
import com.brainstars.computerstore.models.ProductsByCategoryModel;
import com.brainstars.computerstore.repository.ProductRepository;
import com.brainstars.computerstore.service.order.OrderService;
import com.brainstars.computerstore.service.store.StoreServiceImpl;
import com.brainstars.computerstore.utils.MapperHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.brainstars.computerstore.PagingConstansts.DIRECTION;
import static com.brainstars.computerstore.PagingConstansts.ORDER_BY;
import static com.brainstars.computerstore.PagingConstansts.PAGE;
import static com.brainstars.computerstore.PagingConstansts.PAGE_SIZE;
import static com.brainstars.computerstore.ProductConstants.CATEGORY_L;
import static com.brainstars.computerstore.ProductConstants.CATEGORY_M;
import static com.brainstars.computerstore.ProductConstants.DESCRIPTION;
import static com.brainstars.computerstore.ProductConstants.EXISTING_PRODUCT_ID;
import static com.brainstars.computerstore.ProductConstants.NON_EXISTENT_PRODUCT_ID;
import static com.brainstars.computerstore.ProductConstants.NOT_VALID_QUANTITY;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_1;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_1_UPDATED;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_2;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_NAME_3;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_QUANTITY;
import static com.brainstars.computerstore.ProductConstants.PRODUCT_VERSION;
import static com.brainstars.computerstore.ProductConstants.VALID_QUANTITY;
import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StoreServiceTest {

    private final ProductModel productModel = new ProductModel(PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY);
    private final ProductModel updatedProductModel = new ProductModel(PRODUCT_NAME_1_UPDATED, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY);
    private final Product product = new Product(null, PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY, now(), now(), PRODUCT_VERSION);
    private final Product oldProduct = new Product(EXISTING_PRODUCT_ID, PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY, now(), now(), PRODUCT_VERSION);
    private final ProductModel newProductModel = new ProductModel(PRODUCT_NAME_1_UPDATED, null, null, null);
    private final Product updatedProduct = new Product(EXISTING_PRODUCT_ID, PRODUCT_NAME_1_UPDATED, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY, now(), now(), PRODUCT_VERSION);
    private final Pageable pageable = PageRequest.of(PAGE, PAGE_SIZE, Sort.Direction.valueOf(DIRECTION),ORDER_BY);

    private final List<ProductModel> productModels = Arrays.asList(
            new ProductModel(PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY),
            new ProductModel(PRODUCT_NAME_2, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY),
            new ProductModel(PRODUCT_NAME_3, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY)
    );

    private final List<Product> products = Arrays.asList(
            new Product(null, PRODUCT_NAME_1, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY, now(), now(), PRODUCT_VERSION),
            new Product(null, PRODUCT_NAME_2, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY, now(), now(), PRODUCT_VERSION),
            new Product(null, PRODUCT_NAME_3, CATEGORY_L, DESCRIPTION, PRODUCT_QUANTITY, now(), now(), PRODUCT_VERSION)
    );

    private final List<ProductsByCategoryModel> productsByCategories = Arrays.asList(
            new ProductsByCategoryModel(CATEGORY_L, 3),
            new ProductsByCategoryModel(CATEGORY_M, 8)
    );

    @Mock
    private ProductRepository productRepository;

    @Mock
    private OrderService orderService;

    @InjectMocks
    private StoreServiceImpl storeService;

    @Test
    void whenSave_thenProductShouldBeSavedAndReturned() {
        when(productRepository.save(any(Product.class))).thenReturn(product);

        assertThat(storeService.save(productModel)).isEqualTo(productModel);
    }

    @Test
    void whenFindById_thenThrowException() {
        assertThrows(ProductDoesntExistException.class, () ->
                storeService.findById(NON_EXISTENT_PRODUCT_ID));
    }

    @Test
    void whenFindById_thenReturnProduct() {
        when(productRepository.findById(EXISTING_PRODUCT_ID)).thenReturn(java.util.Optional.of(product));

        assertThat(storeService.findById(EXISTING_PRODUCT_ID)).isEqualTo(productModel);
    }

    @Test
    void whenDelete_thenProductShouldBeDeleted() {
        when(productRepository.findById(EXISTING_PRODUCT_ID)).thenReturn(java.util.Optional.of(product));

        storeService.deleteById(EXISTING_PRODUCT_ID);
        verify(productRepository, times(1)).deleteById(EXISTING_PRODUCT_ID);
    }

    @Test
    void whenDelete_thenThrowException() {
        assertThrows(ProductDoesntExistException.class, () ->
                storeService.deleteById(NON_EXISTENT_PRODUCT_ID));
    }

    @Test
    void whenMapFor_thenSuccess() {
        assertThat(MapperHelper.map(oldProduct, newProductModel)).isEqualTo(updatedProduct);
    }

    @Test
    void whenUpdate_thenReturnUpdated() {
        when(productRepository.lockFindById(EXISTING_PRODUCT_ID)).thenReturn(java.util.Optional.of(oldProduct));
        when(productRepository.save(updatedProduct)).thenReturn(updatedProduct);

        assertThat(storeService.update(EXISTING_PRODUCT_ID, updatedProductModel)).isEqualTo(updatedProductModel);
    }

    @Test
    void whenUpdate_thenThrowException() {
        assertThrows(ProductDoesntExistException.class, () ->
                storeService.update(NON_EXISTENT_PRODUCT_ID, updatedProductModel));
    }

    @Test
    void whenGetProducts_thenReturnProducts() {
        when(productRepository.findAll(null, pageable)).
                thenReturn(new PageImpl<>(products));

        var products2 = storeService.getProducts(PAGE, PAGE_SIZE, ORDER_BY, DIRECTION, new ArrayList<>());
        assertThat(products2.size()).isEqualTo(3);
        assertThat(products2.get(0)).isEqualTo(productModels.get(0));
        assertThat(products2.get(1)).isEqualTo(productModels.get(1));
        assertThat(products2.get(2)).isEqualTo(productModels.get(2));
    }

    @Test
    void whenGetProductsByCategory_thenReturnProductsByCategories() {
        when(productRepository.findCategories()).thenReturn(productsByCategories);

        var actual = storeService.getProductsByCategory();
        assertThat(actual.size()).isEqualTo(2);

        var first = actual.get(0);
        assertThat(first.getCategory()).isEqualTo(CATEGORY_L);
        assertThat(first.getProductsAvailable()).isEqualTo(3);

        var second = actual.get(1);
        assertThat(second.getCategory()).isEqualTo(CATEGORY_M);
        assertThat(second.getProductsAvailable()).isEqualTo(8);
    }

    @Test
    void whenOrderProductsWithCorrectQuantity_thenReturnProduct() {
        when(productRepository.lockFindById(EXISTING_PRODUCT_ID)).thenReturn(java.util.Optional.of(product));
        when(productRepository.save(product)).thenReturn(product);

        var actual = storeService.orderProduct(EXISTING_PRODUCT_ID, VALID_QUANTITY);

        assertThat(actual.getQuantity()).isEqualTo(PRODUCT_QUANTITY - VALID_QUANTITY);
    }

    @Test
    void whenOrderProductsWithInCorrectQuantity_thenThrowException() {
        when(productRepository.lockFindById(any(Long.class))).thenReturn(java.util.Optional.of(product));

        assertThrows(InsufficientQuantityException.class, () -> {
            storeService.orderProduct(EXISTING_PRODUCT_ID, NOT_VALID_QUANTITY);
        });
    }

    @Test
    void whenOrderProductsWithMissingProduct_thenThrowException() {
        assertThrows(ProductDoesntExistException.class, () -> storeService.orderProduct(NON_EXISTENT_PRODUCT_ID, VALID_QUANTITY));
    }
}
