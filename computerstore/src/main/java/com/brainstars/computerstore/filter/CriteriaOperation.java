package com.brainstars.computerstore.filter;

import com.brainstars.computerstore.exeption.CriteriaDoesntSupportedException;
import lombok.Getter;

@Getter
public enum CriteriaOperation {
    EQUALS("eq"),
    CONTAINS("co"),
    STARTS_WITH("sw"),
    GREATER_THAN("gt"),
    LESS_THAN("lt");

    private final String operation;

    CriteriaOperation(String operation) {
        this.operation = operation;
    }

    public static CriteriaOperation getOperation(String operation) {
        for (CriteriaOperation value : CriteriaOperation.values()) {
            if (value.getOperation().equalsIgnoreCase(operation))
                return value;
        }

        throw new CriteriaDoesntSupportedException("This filter operation is not supported yet. Operation: " + operation);
    }
}
