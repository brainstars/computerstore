package com.brainstars.computerstore.filter;

import com.brainstars.computerstore.domain.Product;
import com.brainstars.computerstore.exeption.CriteriaDoesntSupportedException;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class ProductSpecification implements Specification<Product> {

    private final transient SearchCriteria searchCriteria;

    public ProductSpecification(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        switch (searchCriteria.getOperation()) {
            case EQUALS:
                return criteriaBuilder.equal(root.get(searchCriteria.getKey()), searchCriteria.getValue());
            case LESS_THAN:
                return criteriaBuilder.lessThan(root.get(searchCriteria.getKey()), searchCriteria.getValue().toString());
            case GREATER_THAN:
                return criteriaBuilder.greaterThan(root.get(searchCriteria.getKey()), searchCriteria.getValue().toString());
            case CONTAINS:
                return criteriaBuilder.like(root.get(searchCriteria.getKey()), "%" + searchCriteria.getValue() + "%");
            case STARTS_WITH:
                return criteriaBuilder.like(root.get(searchCriteria.getKey()), searchCriteria.getValue() + "%");
            default:
                throw new CriteriaDoesntSupportedException("This filter operation is not supported yet. Operation: " + searchCriteria.getOperation());
        }
    }
}
