package com.brainstars.computerstore.filter;

import com.brainstars.computerstore.domain.Product;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Objects;

public class ProductSpecificationBuilder {

    private ProductSpecificationBuilder() {
    }

    public static Specification<Product> build(List<SearchCriteria> searchCriteria) {

        if (Objects.isNull(searchCriteria) || searchCriteria.isEmpty()) {
            return null;
        }

        Specification<Product> result = new ProductSpecification(searchCriteria.get(0));

        for (int i = 1; i < searchCriteria.size(); i++) {
            result = Specification.where(result).and(new ProductSpecification(searchCriteria.get(i)));
        }

        return result;
    }
}
