package com.brainstars.computerstore.exeption;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({InsufficientQuantityException.class})
    public final ResponseEntity<String> productExceptionsHandler() {
        return new ResponseEntity<>("Sorry, NOT enough quantity from this Product.", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ProductDoesntExistException.class})
    public final ResponseEntity<String> handleProductDoesntExistException() {
        return new ResponseEntity<>("Product doesn't exit!", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CriteriaDoesntSupportedException.class)
    public final ResponseEntity<String> handleCriteriaDoesntSupportedException(RuntimeException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<String> globalExceptionHandler(RuntimeException ex) {
        log.error(ex);
        return new ResponseEntity<>("Something bad happened! The problem will be fixed as soon as possible!", HttpStatus.BAD_REQUEST);
    }
}
