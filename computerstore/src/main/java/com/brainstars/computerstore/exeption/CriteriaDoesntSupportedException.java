package com.brainstars.computerstore.exeption;

public class CriteriaDoesntSupportedException extends RuntimeException {

    public CriteriaDoesntSupportedException() {
        super();
    }


    public CriteriaDoesntSupportedException(String message) {
        super(message);
    }


    public CriteriaDoesntSupportedException(String message, Throwable cause) {
        super(message, cause);
    }
}
