package com.brainstars.computerstore.exeption;

public class ProductDoesntExistException extends RuntimeException {

    public ProductDoesntExistException() {
        super();
    }


    public ProductDoesntExistException(String message) {
        super(message);
    }


    public ProductDoesntExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
