package com.brainstars.computerstore.exeption;

public class InsufficientQuantityException extends RuntimeException {

    public InsufficientQuantityException() {
        super();
    }


    public InsufficientQuantityException(String message) {
        super(message);
    }


    public InsufficientQuantityException(String message, Throwable cause) {
        super(message, cause);
    }
}
