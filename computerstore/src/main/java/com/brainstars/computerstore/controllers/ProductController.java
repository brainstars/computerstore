package com.brainstars.computerstore.controllers;

import com.brainstars.computerstore.filter.SearchCriteria;
import com.brainstars.computerstore.models.ProductModel;
import com.brainstars.computerstore.models.ProductsByCategoryModel;
import com.brainstars.computerstore.models.SearchCriteriaModel;
import com.brainstars.computerstore.models.TotalProductsModel;
import com.brainstars.computerstore.service.store.StoreService;
import com.brainstars.computerstore.utils.MapperHelper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("rest-api/products")
@RequiredArgsConstructor
public class ProductController {

    private final StoreService storeService;

    @ApiOperation(value = "Add a product.", response = ProductModel.class)
    @PostMapping
    public ProductModel save(@Parameter(description = "Saved product.", required = true)
                             @RequestBody ProductModel productModel) {
        return storeService.save(productModel);
    }

    @ApiOperation(value = "Search a product by ID.", response = ProductModel.class)
    @GetMapping("/{id}")
    public ProductModel getProduct(@ApiParam(value = "The product id", required = true)
                                   @PathVariable long id) {
        return storeService.findById(id);
    }

    @ApiOperation(value = "Update a product by ID.", response = ProductModel.class)
    @PatchMapping("/{id}")
    public ProductModel update(@ApiParam(value = "Product identifier.", required = true)
                               @PathVariable long id,
                               @Parameter(description = "Updated Product.")
                               @RequestBody ProductModel productModel) {
        return storeService.update(id, productModel);
    }

    @ApiOperation(value = "Delete a product by ID.", response = void.class)
    @DeleteMapping("/{id}")
    public void delete(@ApiParam(value = "Product identifier.", required = true)
                       @PathVariable long id) {
        storeService.deleteById(id);
    }

    @ApiOperation(value = "View a list of products.",
            notes = "There is a paging and the list can be filtered by criteria.",
            response = TotalProductsModel.class)
    @GetMapping
    public TotalProductsModel getAllProducts(
            @ApiParam(value = "Sorting by")
            @RequestParam(defaultValue = "id") String orderBy,
            @ApiParam(value = "Direction of sorting.")
            @RequestParam(defaultValue = "ASC") String direction,
            @ApiParam(value = "Number of page.")
            @RequestParam(defaultValue = "0") int page,
            @ApiParam(value = "Size of pages.")
            @RequestParam(defaultValue = "10") int pageSize,
            @Parameter(description = "Filtering the list of products by criteria.")
            @RequestBody(required = false) List<SearchCriteriaModel> searchCriteriaModels) {

        List<SearchCriteria> criteriaList = MapperHelper.toSearchCriteria(searchCriteriaModels);
        List<ProductModel> products = storeService.getProducts(page - 1, pageSize, orderBy, direction, criteriaList);

        return new TotalProductsModel(products.size(), products);
    }

    @ApiOperation(value = "View a list of available products by their categories.", response = ProductsByCategoryModel.class)
    @GetMapping("/categories")
    public List<ProductsByCategoryModel> getProductsByCategories() {
        return storeService.getProductsByCategory();
    }

    @ApiOperation(value = "Order a product.", response = ProductModel.class)
    @PostMapping("/{id}/order/{amount}")
    public ProductModel orderProducts(
            @ApiParam(value = "Product identifier.", required = true)
            @PathVariable long id,
            @ApiParam(value = "Amount of product that should be ordered.", required = true)
            @PathVariable int amount) {
        var product = storeService.orderProduct(id, amount);

        return product;
    }
}
