package com.brainstars.computerstore.controllers;

import com.brainstars.computerstore.service.client.ClientService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rest-api/clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @ApiOperation(value = "View a list of clients in json.", response = String.class)
    @GetMapping
    public ResponseEntity<String> getClients() {
        return clientService.getClients();
    }
}
