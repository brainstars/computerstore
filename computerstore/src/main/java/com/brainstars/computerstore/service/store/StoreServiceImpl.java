package com.brainstars.computerstore.service.store;

import com.brainstars.computerstore.domain.Product;
import com.brainstars.computerstore.exeption.InsufficientQuantityException;
import com.brainstars.computerstore.exeption.ProductDoesntExistException;
import com.brainstars.computerstore.filter.ProductSpecificationBuilder;
import com.brainstars.computerstore.filter.SearchCriteria;
import com.brainstars.computerstore.models.ProductModel;
import com.brainstars.computerstore.models.ProductsByCategoryModel;
import com.brainstars.computerstore.repository.ProductRepository;
import com.brainstars.computerstore.service.order.OrderService;
import com.brainstars.computerstore.utils.MapperHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class StoreServiceImpl implements StoreService {

    private static final int DEFAULT_PAGE_NUM = 0;
    private static final int DEFAULT_PAGE_SIZE = 10;

    private final ProductRepository productRepository;
    private final OrderService orderService;

    public StoreServiceImpl(ProductRepository productRepository,
                            OrderService orderService) {
        this.productRepository = productRepository;
        this.orderService = orderService;
    }


    @Override
    public ProductModel save(ProductModel productModel) {
        Product savedProduct = productRepository.save(MapperHelper.mapToProduct(productModel));

        return MapperHelper.mapToProductModel(savedProduct);
    }

    @Override
    public void deleteById(long id) {
        findById(id);
        productRepository.deleteById(id);
    }

    @Override
    public ProductModel findById(long id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isEmpty()) {
            throw new ProductDoesntExistException();
        }

        return MapperHelper.mapToProductModel(product.get());
    }

    @Override
    @Transactional
    public ProductModel update(long id, ProductModel productModel) {
        Optional<Product> oldProduct = productRepository.lockFindById(id);
        if (oldProduct.isEmpty()) {
            throw new ProductDoesntExistException();
        }

        Product updatedProduct = productRepository.save(MapperHelper.map(oldProduct.get(), productModel));

        return MapperHelper.mapToProductModel(updatedProduct);
    }

    @Override
    public List<ProductModel> getProducts(int page, int pageSize, String orderBy, String direction, List<SearchCriteria> criteriaList) {
        if (page < 0) {
            page = DEFAULT_PAGE_NUM;
        }

        if (pageSize < 0) {
            pageSize = DEFAULT_PAGE_SIZE;
        }

        Specification<Product> spec = ProductSpecificationBuilder.build(criteriaList);
        Pageable pageable =
                PageRequest.of(page, pageSize, Sort.Direction.valueOf(direction), orderBy);

        List<Product> products = productRepository.findAll(spec, pageable).getContent();

        if (products.isEmpty())
            return Collections.emptyList();

        return MapperHelper.mapToProductModels(products);
    }

    @Override
    @Transactional
    public ProductModel orderProduct(long id, int amount) {
        Optional<Product> product = productRepository.lockFindById(id);

        if (product.isEmpty()) {
            throw new ProductDoesntExistException();
        }

        Product productOrdered = product.get();
        int productQuantity = productOrdered.getQuantity();
        if (amount > 0 && amount <= productQuantity) {
            productOrdered.setQuantity(productQuantity - amount);
            productRepository.save(productOrdered);
            orderService.postOrders(id, amount);

            return MapperHelper.mapToProductModel(productOrdered);
        } else
            throw new InsufficientQuantityException();
    }


    @Override
    public List<ProductsByCategoryModel> getProductsByCategory() {
        return productRepository.findCategories();
    }
}
