package com.brainstars.computerstore.service.order;

public interface OrderService {
    void postOrders(long id, int amount);
}
