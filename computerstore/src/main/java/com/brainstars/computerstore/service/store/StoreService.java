package com.brainstars.computerstore.service.store;


import com.brainstars.computerstore.filter.SearchCriteria;
import com.brainstars.computerstore.models.ProductModel;
import com.brainstars.computerstore.models.ProductsByCategoryModel;

import java.util.List;

public interface StoreService {
    ProductModel save(ProductModel productModel);

    void deleteById(long id);

    ProductModel findById(long id);

    ProductModel update(long id, ProductModel productModel);

    List<ProductModel> getProducts(int page, int pageSize, String orderBy, String direction, List<SearchCriteria> criteriaList);

    ProductModel orderProduct(long id, int amount);

    List<ProductsByCategoryModel> getProductsByCategory();
}
