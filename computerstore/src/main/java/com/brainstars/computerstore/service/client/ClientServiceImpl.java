package com.brainstars.computerstore.service.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ClientServiceImpl implements ClientService {

    private final String username;

    private final String password;

    private final String url;

    private final RestTemplate restTemplate;

    public ClientServiceImpl(@Value("${clients.credentials.username:admin}") String username,
                             @Value("${clients.credentials.password:admin}") String password,
                             @Value("${clients.url:http://localhost:8080/rest-api/clients}") String url, RestTemplate restTemplate) {
        this.username = username;
        this.password = password;
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> getClients() {
        restTemplate.getInterceptors().add(
                new BasicAuthenticationInterceptor(username, password));

        return restTemplate.getForEntity(url, String.class);
    }
}
