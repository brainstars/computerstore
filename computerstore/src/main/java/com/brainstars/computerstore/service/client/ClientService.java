package com.brainstars.computerstore.service.client;

import org.springframework.http.ResponseEntity;

public interface ClientService {
    ResponseEntity<String> getClients();
}
