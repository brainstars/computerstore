package com.brainstars.computerstore.service.order;

import com.brainstars.computerstore.models.OrderModel;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderServiceImpl implements OrderService {

    private final String applicationName;
    private final RestTemplate restTemplate;
    private final EurekaClient eurekaClient;

    public OrderServiceImpl(RestTemplate restTemplate,
                            @Qualifier("eurekaClient") EurekaClient eurekaClient,
                            @Value("${eureka.client.name}") String applicationName) {
        this.restTemplate = restTemplate;
        this.eurekaClient = eurekaClient;
        this.applicationName = applicationName;
    }

    @Override
    public void postOrders(long id, int amount) {
        InstanceInfo instance = eurekaClient.getNextServerFromEureka(applicationName, false);
        restTemplate.postForEntity(instance.getHomePageUrl() + "rest-api/orders", new OrderModel(id, amount), String.class);
    }
}
