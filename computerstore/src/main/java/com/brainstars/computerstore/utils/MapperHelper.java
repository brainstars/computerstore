package com.brainstars.computerstore.utils;

import com.brainstars.computerstore.domain.Product;
import com.brainstars.computerstore.filter.CriteriaOperation;
import com.brainstars.computerstore.filter.SearchCriteria;
import com.brainstars.computerstore.models.ProductModel;
import com.brainstars.computerstore.models.SearchCriteriaModel;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MapperHelper {

    private static final ModelMapper mapper = new ModelMapper();

    private MapperHelper() {

    }

    public static Product map(Product oldProduct, ProductModel newProduct) {
        if (Objects.nonNull(newProduct.getName()) && !newProduct.getName().equals(oldProduct.getName())) {
            oldProduct.setName(newProduct.getName());
        }

        if (Objects.nonNull(newProduct.getCategory()) && !newProduct.getCategory().equals(oldProduct.getCategory())) {
            oldProduct.setCategory(newProduct.getCategory());
        }

        if (Objects.nonNull(newProduct.getDescription()) && !newProduct.getDescription().equals(oldProduct.getDescription())) {
            oldProduct.setDescription(newProduct.getDescription());
        }

        if (Objects.nonNull(newProduct.getQuantity()) && !newProduct.getQuantity().equals(oldProduct.getQuantity())) {
            oldProduct.setQuantity(newProduct.getQuantity());
        }

        return oldProduct;
    }

    public static Product mapToProduct(ProductModel productModel) {
        return mapper.map(productModel, Product.class);
    }

    public static ProductModel mapToProductModel(Product product) {
        return mapper.map(product, ProductModel.class);
    }

    public static List<ProductModel> mapToProductModels(List<Product> products) {

        return products.stream().map(p -> mapper.map(p, ProductModel.class)).collect(Collectors.toList());
    }

    public static List<SearchCriteria> toSearchCriteria(List<SearchCriteriaModel> searchCriteriaModels) {
        if (Objects.isNull(searchCriteriaModels) || searchCriteriaModels.isEmpty())
            return Collections.emptyList();

        List<SearchCriteria> searchCriteriaList = new ArrayList<>();

        for (SearchCriteriaModel model : searchCriteriaModels) {
            searchCriteriaList.add(new SearchCriteria(
                    model.getKey(),
                    CriteriaOperation.getOperation(model.getOperation()),
                    model.getValue()
            ));
        }
        return searchCriteriaList;
    }

    public static SearchCriteria toSearchCriteria(SearchCriteriaModel searchCriteriaModels) {
        return new SearchCriteria(
                searchCriteriaModels.getKey(),
                CriteriaOperation.getOperation(searchCriteriaModels.getOperation()),
                searchCriteriaModels.getValue());
    }
}
