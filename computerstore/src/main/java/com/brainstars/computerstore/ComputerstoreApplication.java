package com.brainstars.computerstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ComputerstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComputerstoreApplication.class, args);
	}

}
