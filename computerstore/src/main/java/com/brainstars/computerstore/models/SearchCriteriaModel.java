package com.brainstars.computerstore.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Details about SearchCriteriaModel")
public class SearchCriteriaModel {

    @ApiModelProperty(value = "The field name", notes = "ex: name, quantity... etc")
    private String key;

    @ApiModelProperty(value = "The operation", notes = "ex: lt(less than), eq(equals), sw(start with)... etc")
    private String operation;

    @ApiModelProperty(value = "The field value", notes = "ex: Dell 5401 (name) , 14(quantity)... etc")
    private String value;
}
