package com.brainstars.computerstore.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@ApiModel(description = "Details about ProductByCategoryModel")
public class ProductsByCategoryModel {

    @ApiModelProperty("A category.")
    private String category;

    @ApiModelProperty("Total number of a given category.")
    private long productsAvailable;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductsByCategoryModel that = (ProductsByCategoryModel) o;
        return productsAvailable == that.productsAvailable &&
                Objects.equals(category, that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category, productsAvailable);
    }
}
