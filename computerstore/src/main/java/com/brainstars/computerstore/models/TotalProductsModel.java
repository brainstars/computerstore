package com.brainstars.computerstore.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ApiModel(description = "All Products and their total number.")
public class TotalProductsModel {

    @ApiModelProperty("Number of all products.")
    private long totalRecords;

    @ApiModelProperty("All products.")
    private List<ProductModel> productModels;
}
