package com.brainstars.computerstore.repository;

import com.brainstars.computerstore.domain.Product;
import com.brainstars.computerstore.models.ProductsByCategoryModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    @Query("SELECT new com.brainstars.computerstore.models.ProductsByCategoryModel(p.category,COUNT(p))" +
            "FROM Product p " +
            "GROUP BY p.category")
    List<ProductsByCategoryModel> findCategories();

    @Lock(LockModeType.PESSIMISTIC_FORCE_INCREMENT)
    @Query("SELECT p FROM Product p WHERE p.id = ?1")
    Optional<Product> lockFindById(Long id);
}
