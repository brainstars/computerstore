1). You need PostgreSQL db.

2). Create loging user with name:computerstore and password: computerstore.

3). Set user privileges "Can login".

4). Create new DB with name:computerstore and owner: computerstore

5). Make sure you are running postgres on port 5432

6). Clone the repository.

7). Checkout to master branch.

8). Start eureka server.

9). Go in the project folder and run in terminal: mvn spring-boot:run

10). Check the db config in application.properites and more specifically db port(Should be the same as postgresql). 

11). Server runs at port 8080. ( Fisrt should be started eureka server.)

12).Add new product: http://localhost:8080/rest-api/products/ (POST)

JSON: { "name":"ASUS", "category":"Laptop", "description":"desc", "quantity":35 }

13).Update product: http://localhost:8080/rest-api/products/{id} (PATCH)

JSON: {"name":"Asus", "description":"new description", "category":"new category" }

14).Delete product: http://localhost:8080/product/{id} (DELETE)

JSON: { "id":1001 }

15).Get a product: http://localhost:8080/rest-api/products/{id}

16).Get all products: http://localhost:8080/rest-api/products/?orderBy={name}&direction={direction}&page={page}&pageSize={pageSize} (GET)

JSON: [{ "key": "name", "operation": "eq", "value": "Dell U2413" }]

17).Get all categories: http://localhost:8080/categories (GET)

18).Make an order: http://localhost:8080/rest-api/products/{id}/order/{amount}

19).Get clients: http://localhost:8080/rest-api/clients/